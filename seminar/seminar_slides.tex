\documentclass[xcolor={dvipsnames}]{beamer}
\beamertemplatenavigationsymbolsempty
\addtobeamertemplate{navigation symbols}{}{%
    \usebeamerfont{footline}%
    \usebeamercolor[fg]{footline}%
    \hspace{1em}%
    \insertframenumber/\inserttotalframenumber
}

\usepackage[UKenglish]{babel}
\usepackage{graphicx}
\usepackage{transparent}
\usepackage{tikz}
\usepackage{subcaption}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage[linesnumbered,ruled,vlined]{algorithm2e}
\usepackage{multirow}
\usepackage{makecell}
\usepackage{siunitx}

\usepackage[utf8]{inputenc}
\usetheme{default}

\hypersetup{
   plainpages=false,
   unicode,
   %pdftitle={\@title},
   pdfauthor={A. S. Gwilt},
   hidelinks
}

\title{Single-Row Circuit Placement and Routing Optimisation (III)}
\author{Andreas Gwilt}
\date{26 October 2022}

\usetikzlibrary{quotes,arrows.meta}
\input{../tikz_commands.tex}

\newcommand*{\metalbelow}{\texttt{metal\_below}}
\newcommand*{\viabelow}{\texttt{via\_below}}
\newcommand*{\metalvert}{\texttt{metal\_\allowbreak vert}}
\newcommand*{\viabetween}{\texttt{via\_between}}
\newcommand*{\metalhor}{\texttt{metal\_hor}}

\newcommand*{\prev}{\textup{prev}}

\begin{document}

\begin{frame}[plain]
    \maketitle
\end{frame}

\begin{frame}{Overview}
  \tableofcontents
\end{frame}

\section{Problem Formulation}
\begin{frame}[t]{Preliminaries}
\begin{itemize}
  \item \textbf{Leaf cell:} simple circuit, up to ca. 30 transistors
  \item Chip logic made using library of leaf cell designs
  \item Cells are placed in rows, and connected in the routing step
  \item Routing layers we care about (from top):
    \textcolor{violet}{\metalhor{}},
    \textcolor{violet}{\viabetween{}},
    \textcolor{Green}{\metalvert{}},
    \textcolor{Green}{\viabelow{}} and
    \textcolor{RoyalBlue}{\metalbelow{}}
\end{itemize}
\only<1>{
\begin{figure}[h]
  \centering
  \includegraphics[width=0.75\textwidth]{../images/layers.png}
  \caption{Layers in GlobalFoundries' abandoned \qty{7}{nm} technology}\label{fig:layers}
\end{figure}
}
\uncover<2->{
\begin{figure}[h]
  \centering
  %\vspace{-0.5em}
  \resizebox{\textwidth}{!}{%
    \begin{tikzpicture}
      \drawmetalhortrackpattern{27}{8}
      \drawmetalverttrackpattern{27}{8}

      \draw (0,0) rectangle (\Cmetalvertpitch*6,\Cmetalhorpitch*8);

      \only<2>{
        \drawcapiece{0.5}{0}{\Cmetalhorpitch*3.3}
        \drawcapiece{0.5}{\Cmetalhorpitch*4.7}{\Cmetalhorpitch*8}
        \drawpcpiece{1}{0}{\Cmetalhorpitch*8}
        \drawcapiece{1.5}{0}{\Cmetalhorpitch*3.3}
        \drawcapiece{1.5}{\Cmetalhorpitch*4.7}{\Cmetalhorpitch*8}
        \drawpcpiece{2}{0}{\Cmetalhorpitch*8}
        \drawcapiece{2.5}{0}{\Cmetalhorpitch*3.3}
        \drawcapiece{2.5}{\Cmetalhorpitch*4.7}{\Cmetalhorpitch*8}
        \drawpcpiece{3}{0}{\Cmetalhorpitch*8}
        \drawcapiece{3.5}{0}{\Cmetalhorpitch*3.3}
        \drawcapiece{3.5}{\Cmetalhorpitch*4.7}{\Cmetalhorpitch*8}

        \drawcb{0.5}{1.2*\Cmetalhorpitch}
        \drawcb{0.5}{7.4}
        \drawcb{1}{6.8*\Cmetalhorpitch}
        \drawcb{1.5}{2.6*\Cmetalhorpitch}
        \drawcb{2}{4*\Cmetalhorpitch}
        \drawcb{2.5}{5.4*\Cmetalhorpitch}
        \drawcb{2.5}{1.2*\Cmetalhorpitch}
        \drawcb{3}{6.8*\Cmetalhorpitch}
        \drawcb{3.5}{7.4}
        \drawcb{3.5}{0.3}

        \filldraw [metalbelowcolour, draw=metalbelowcolour!40!black, opacity=0.5] (0,-0.5) rectangle (\Cmetalvertpitch*6,0.5);%
        \filldraw [metalbelowcolour, draw=metalbelowcolour!40!black, opacity=0.5] (0,\Cmetalhorpitch*8-0.5) rectangle (\Cmetalvertpitch*6,\Cmetalhorpitch*8+0.5);%
        \drawmetalbelow{0.4}{5}{1.2*\Cmetalhorpitch}
        \drawmetalbelow{0.4+\Cpcpitch}{5.8}{2.6*\Cmetalhorpitch}
        \drawmetalbelow{2*\Cpcpitch-1}{5}{4*\Cmetalhorpitch}
        \drawmetalbelow{2*\Cpcpitch}{6}{5.4*\Cmetalhorpitch}
        \drawmetalbelow{2*\Cpcpitch+1}{6.8}{6.8*\Cmetalhorpitch}
        \drawmetalbelow{0.4}{\Cpcpitch+1}{6.8*\Cmetalhorpitch}
      }
      \drawviabelow{1}{6.8*\Cmetalhorpitch}
      \drawviabelow{3}{4*\Cmetalhorpitch}
      \drawviabelow{4}{2.6*\Cmetalhorpitch}
      \drawviabelow{4}{5.4*\Cmetalhorpitch}
      \drawviabelow{5}{6.8*\Cmetalhorpitch}

      \drawmetalvertconnection{4}{2}{6}

      \uncover<4->{
      \draw (\Cmetalvertpitch*6,0) rectangle (\Cmetalvertpitch*13,\Cmetalhorpitch*8);
      \draw (\Cmetalvertpitch*15,0) rectangle (\Cmetalvertpitch*21,\Cmetalhorpitch*8);
      \draw (\Cmetalvertpitch*21,0) rectangle (\Cmetalvertpitch*27,\Cmetalhorpitch*8);

      \drawviabelow{7}{3.7}
      \drawviabelow{16}{3}
      \drawviabelow{18}{4}
      \drawviabelow{22}{3}
      \drawviabelow{25}{4}

      \drawmetalvertconnection{11}{2}{6}
      \drawmetalvertconnection{20}{2}{6}
      \drawmetalvertconnection{26}{2}{6}
      }

      \uncover<5->{
      \drawmetalvertconnection{1}{6}{6}
      \drawmetalvert{3}{1}{5}
      \drawmetalvertconnection{5}{2}{6}
      \drawmetalvertconnection{7}{4}{4}
      \drawmetalvertconnection{8}{2}{6}
      \drawmetalvertconnection{14}{2}{6}
      \drawmetalvertconnection{16}{3}{3}
      \drawmetalvertconnection{18}{4}{4}
      \drawmetalvertconnection{22}{3}{3}
      \drawmetalvertconnection{25}{4}{4}

      \drawviabetween{1}{5}
      \drawviabetween{4}{3}
      \drawviabetween{5}{1}
      \drawviabetween{5}{6}
      \drawviabetween{7}{4}
      \drawviabetween{8}{2}
      \drawviabetween{8}{6}
      \drawviabetween{11}{7}
      \drawviabetween{14}{1}
      \drawviabetween{14}{7}
      \drawviabetween{16}{3}
      \drawviabetween{18}{4}
      \drawviabetween{20}{3}
      \drawviabetween{20}{5}
      \drawviabetween{22}{3}
      \drawviabetween{25}{4}

      \drawmetalhorconnection{0}{5}{1}
      \drawmetalhorconnection{1}{20}{5}
      \drawmetalhorconnection{4}{16}{3}
      \drawmetalhorconnection{5}{8}{6}
      \drawmetalhorconnection{3}{7}{4}
      \drawmetalhorconnection{11}{14}{7}
      \drawmetalhorconnection{14}{27}{1}
      \drawmetalhorconnection{17}{18}{4}
      \drawmetalhorconnection{19}{22}{3}
      \drawmetalhorconnection{25}{27}{4}
      }
      \uncover<4->{
      \drawmetalhorconnection{9}{11}{1}
      \drawmetalhorconnection{7}{9}{2}
      }
    \end{tikzpicture}
  }
\end{figure}
}
\uncover<6->{
\begin{itemize}
  \item \textcolor{violet}{\metalhor{}} must lie on \textbf{HTracks}
  \item \textcolor{Green}{\metalvert{}} must lie on \textbf{VTracks}
\end{itemize}
(both visible as dashed lines)
}
\end{frame}

\begin{frame}{The \textsc{Single Row Track Optimisation Problem}}
  \begin{description}
    \item[Instance:]
      \begin{itemize}
        \item A list of $n$ leaf cells, each having a width $W_i\in\mathbb{N}$, existing wiring, pins, and a placement
          grid
        \item Design rule values
        \item Track patterns (regular VTrack grid, $k$ HTracks)
        \item Global left/right boundary pins
        \item Global VTrack blockages
      \end{itemize}
    \item[Task:] Find a (minimum width) cell placement $p:\{1,\ldots,n\}\to\mathbb{N}_0$ and a (minimum cost) routing
      $\mathcal{R}$ such that
      \begin{itemize}
        \item $\mathcal{R}$ has no shorts and fulfils all design rules
        \item For each net $N$, $\exists$ conn. comp. of $\mathcal{R}$ connecting to at least one shape of each pin of
          net $N$
      \end{itemize}
  \end{description}

\end{frame}

\begin{frame}{Comparison to \textsc{Single Row Placement with Fixed Ordering}}
  \begin{itemize}
    \item Clumping Algorithm: Quickly finds a non-overlapping single row placement minimising some cost function
    \item Unfortunate cell placement can force detours in routing
    \item[$\leadsto$] \textsc{Track Optimisation Problem}: Compute placement and routing simultaneously
    \item This algorithm:
    \begin{itemize}
      \item Finds minimal routing for its placement
      \item Placement can be suboptimal, but usually is optimal
      \item Linear-ish running time:
        \[\mathcal{O}\left(\left(\sum_{i=1}^n W_i\right) + (\#\textup{VTrack blockages})\cdot\max_{i=1}^n W_i^2\right)\]
    \end{itemize}
  \end{itemize}

\end{frame}

\section{The Routing Model}
\begin{frame}{Restricted routings {\transparent{0.5}(assume a fixed placement for now)}}
  \begin{itemize}
    \item \metalhor{} only on grid edges, with overhang for \viabetween{}:
      \begin{figure}
        \centering
        \begin{subfigure}{0.4\textwidth}
          \resizebox{\textwidth}{!}{%
            \begin{tikzpicture}
              \node at (0,-0.8) {\color{gray} \textbullet};
              \node at (1,-0.8) {\textbullet};
              \node at (2,-0.8) {\textbullet};
              \node at (3,-0.8) {\textbullet};
              \node at (4,-0.8) {\color{gray} \textbullet};

              \node at (0,0) {\color{gray} \textbullet};
              \node at (1,0) {\color{gray} \textbullet};
              \node at (2,0) {\textbullet};
              \node at (3,0) {\textbullet};
              \node at (4,0) {\color{gray} \textbullet};

              \node at (0,0.8) {\textbullet};
              \node at (1,0.8) {\textbullet};
              \node at (2,0.8) {\color{gray} \textbullet};
              \node at (3,0.8) {\color{gray} \textbullet};
              \node at (4,0.8) {\color{gray} \textbullet};

              \draw [] (2,0) -- (3,0);
              \draw [] (0,0.8) -- (1,0.8);
              \draw [] (1,-0.8) -- (2,-0.8);
              \draw [] (2,-0.8) -- (3,-0.8);
            \end{tikzpicture}
          }
        \end{subfigure}%
        \hspace{1em}$\leadsto$\hspace{1em}%
        \begin{subfigure}{0.4\textwidth}
          \resizebox{\textwidth}{!}{%
            \begin{tikzpicture}
              \node at (0,-0.8) {\color{gray} \textbullet};
              \node at (1,-0.8) {\textbullet};
              \node at (2,-0.8) {\textbullet};
              \node at (3,-0.8) {\textbullet};
              \node at (4,-0.8) {\color{gray} \textbullet};

              \node at (0,0) {\color{gray} \textbullet};
              \node at (1,0) {\color{gray} \textbullet};
              \node at (2,0) {\textbullet};
              \node at (3,0) {\textbullet};
              \node at (4,0) {\color{gray} \textbullet};

              \node at (0,0.8) {\textbullet};
              \node at (1,0.8) {\textbullet};
              \node at (2,0.8) {\color{gray} \textbullet};
              \node at (3,0.8) {\color{gray} \textbullet};
              \node at (4,0.8) {\color{gray} \textbullet};

              \draw [] (2,0) -- (3,0);
              \draw [] (0,0.8) -- (1,0.8);
              \draw [] (1,-0.8) -- (2,-0.8);
              \draw [] (2,-0.8) -- (3,-0.8);

              \drawmetalhor{0.4}{3.6}{-0.8}
              \drawmetalhor{-0.6}{1.6}{0.8}
              \drawmetalhor{1.4}{3.6}{0}
            \end{tikzpicture}
          }
        \end{subfigure}
      \end{figure}
    \item No two \metalhor{} edges of the same net at the same VTrack
    \item Each net can only start up to a constant number of VTracks before its first pinshape
    \item At most one \metalvert{} piece per VTrack
  \end{itemize}

  \vfill

  \textbf{Note:} Only the \metalhor{} grid is a fundamental assumption.
\end{frame}

\begin{frame}[t]{Routing slices {\transparent{0.5}(assume a fixed placement for now)}}
  Restricted Routing can be split naturally into slices:

  \begin{figure}[h]
    \centering
    %\caption{Example of a complete solved instance}\label{fig:complete_sol}
    \resizebox{\textwidth}{!}{%
      \begin{tikzpicture}
        \only<1>{
        \drawmetalverttrackpattern{7}{5}
        \drawvertexgrid{7}{5}

        \drawblackpath{0}{1}{0}
        \drawblackpath{5}{6}{1}
        \drawblackpath{0}{7}{2}
        \drawblackpath{2}{4}{3}
        \drawblackpath{1}{5}{5}

        \drawviabelow{0}{3-\Cmetalhorpitch}
        \drawviabelow{4}{3.7-\Cmetalhorpitch}

        \drawmetalvert{0}{0}{4}
        \drawmetalvertconnection{1}{1}{4}
        \drawmetalvertconnection{2}{3}{3}
        \drawmetalvertconnection{4}{3}{3}
        \drawmetalvertconnection{5}{2}{4}

        \drawviabetween{0}{2}
        \drawviabetween{1}{0}
        \drawviabetween{1}{5}
        \drawviabetween{2}{3}
        \drawviabetween{4}{3}
        \drawviabetween{5}{1}
        \drawviabetween{5}{5}

        \drawmetalhorconnection{0}{1}{0}
        \drawmetalhorconnection{0}{7}{2}
        \drawmetalhorconnection{1}{5}{5}
        \drawmetalhorconnection{2}{4}{3}
        \drawmetalhorconnection{5}{6}{1}
        }

        \uncover<2->{
        \drawslicebox{0}{5}
        \drawslicebox{4}{5}
        \drawslicebox{8}{5}
        \drawslicebox{12}{5}
        \drawslicebox{16}{5}
        \drawslicebox{20}{5}
        \drawslicebox{24}{5}

        \drawgreyvertexstack{0}{5}
        \drawblackedge{0}{0}
        \drawblackedge{0}{2}

        \drawgreyvertexstack{4}{5}
        \drawblackedge{4}{2}
        \drawblackedge{4}{5}

        \drawgreyvertexstack{8}{5}
        \drawblackedge{8}{2}
        \drawblackedge{8}{3}
        \drawblackedge{8}{5}

        \drawgreyvertexstack{12}{5}
        \drawblackedge{12}{2}
        \drawblackedge{12}{3}
        \drawblackedge{12}{5}

        \drawgreyvertexstack{16}{5}
        \drawblackedge{16}{2}
        \drawblackedge{16}{5}

        \drawgreyvertexstack{20}{5}
        \drawblackedge{20}{1}
        \drawblackedge{20}{2}

        \drawgreyvertexstack{24}{5}
        \drawblackedge{24}{2}

        \drawviabelow{0}{3-\Cmetalhorpitch}
        \drawviabelow{16}{3.7-\Cmetalhorpitch}

        \drawmetalvert{0}{0}{4}
        \drawmetalvertconnection{4}{1}{4}
        \drawmetalvertconnection{8}{3}{3}
        \drawmetalvertconnection{16}{3}{3}
        \drawmetalvertconnection{20}{2}{4}

        \drawviabetween{0}{2}
        \drawviabetween{4}{0}
        \drawviabetween{4}{5}
        \drawviabetween{8}{3}
        \drawviabetween{16}{3}
        \drawviabetween{20}{1}
        \drawviabetween{20}{5}

        \drawmetalhorconnection{0}{1}{0}
        \drawmetalhorconnection{0}{1}{2}

        \drawmetalhorconnection{4}{5}{2}
        \drawmetalhorconnection{4}{5}{5}

        \drawmetalhorconnection{8}{9}{2}
        \drawmetalhorconnection{8}{9}{3}
        \drawmetalhorconnection{8}{9}{5}

        \drawmetalhorconnection{12}{13}{2}
        \drawmetalhorconnection{12}{13}{3}
        \drawmetalhorconnection{12}{13}{5}

        \drawmetalhorconnection{16}{17}{2}
        \drawmetalhorconnection{16}{17}{5}

        \drawmetalhorconnection{20}{21}{1}
        \drawmetalhorconnection{20}{21}{2}

        \drawmetalhorconnection{24}{25}{2}

        \draw [<-, red] (\Cmetalvertpitch*2,2.5) -- (\Cmetalvertpitch*3,2.5);
        \draw [<-, red] (\Cmetalvertpitch*6,2.5) -- (\Cmetalvertpitch*7,2.5);
        \draw [<-, red] (\Cmetalvertpitch*10,2.5) -- (\Cmetalvertpitch*11,2.5);
        \draw [<-, red] (\Cmetalvertpitch*14,2.5) -- (\Cmetalvertpitch*15,2.5);
        \draw [<-, red] (\Cmetalvertpitch*18,2.5) -- (\Cmetalvertpitch*19,2.5);
        \draw [<-, red] (\Cmetalvertpitch*22,2.5) -- (\Cmetalvertpitch*23,2.5);
        }

        \uncover<3->{
        \begin{scope}[shift={(0,-7)}]
          \drawslicebox{4}{5}
          \drawslicebox{8}{5}
          \drawslicebox{12}{5}
          \drawslicebox{16}{5}

          \drawgreyvertexstack{4}{5}
          \drawblackedge{4}{0}
          \drawblackedge{4}{2}
          \drawmetalhorconnection{4}{5}{0}
          \drawmetalhorconnection{4}{5}{2}

          \drawgreyvertexstack{8}{5}
          \drawblackedge{8}{0}
          \drawblackedge{8}{2}
          \drawblackedge{8}{3}
          \drawmetalvertconnection{8}{3}{3}
          \drawviabetween{8}{3}
          \drawmetalhorconnection{8}{9}{0}
          \drawmetalhorconnection{8}{9}{2}
          \drawmetalhorconnection{8}{9}{3}

          \drawgreyvertexstack{12}{5}
          \drawblackedge{12}{0}
          \drawblackedge{12}{2}
          \drawblackedge{12}{3}
          \drawmetalhorconnection{12}{13}{0}
          \drawmetalhorconnection{12}{13}{2}
          \drawmetalhorconnection{12}{13}{3}

          \drawgreyvertexstack{16}{5}
          \drawblackedge{16}{0}
          \drawblackedge{16}{2}
          \drawviabelow{16}{3.7-\Cmetalhorpitch}
          \drawmetalvertconnection{16}{3}{3}
          \drawviabetween{16}{3}
          \drawmetalhorconnection{16}{17}{0}
          \drawmetalhorconnection{16}{17}{2}

          \draw [<-, red] (\Cmetalvertpitch*2,8) -- (\Cmetalvertpitch*3,2.5);
          \draw [<-, red] (\Cmetalvertpitch*6,2.5) -- (\Cmetalvertpitch*7,2.5);
          \draw [<-, red] (\Cmetalvertpitch*10,2.5) -- (\Cmetalvertpitch*11,2.5);
          \draw [<-, red] (\Cmetalvertpitch*14,2.5) -- (\Cmetalvertpitch*15,2.5);
          \draw [<-, red] (\Cmetalvertpitch*18,2.5) -- (\Cmetalvertpitch*19,2.5) node [right] {\Huge dead end};
        \end{scope}

        \begin{scope}[shift={(0,-14)}]
          \drawslicebox{12}{5}
          \drawslicebox{16}{5}
          \drawslicebox{20}{5}
          \drawslicebox{24}{5}

          \drawgreyvertexstack{12}{5}
          \drawblackedge{12}{2}
          \drawblackedge{12}{3}
          \drawblackedge{12}{5}
          \drawmetalvertconnection{12}{1}{4}
          \drawviabetween{12}{0}
          \drawviabetween{12}{5}
          \drawmetalhorconnection{12}{13}{2}
          \drawmetalhorconnection{12}{13}{3}
          \drawmetalhorconnection{12}{13}{5}

          \drawgreyvertexstack{16}{5}
          \drawblackedge{16}{2}
          \drawblackedge{16}{5}
          \drawviabelow{16}{3.7-\Cmetalhorpitch}
          \drawmetalvertconnection{16}{3}{3}
          \drawviabetween{16}{3}
          \drawmetalhorconnection{16}{17}{2}
          \drawmetalhorconnection{16}{17}{5}

          \drawgreyvertexstack{20}{5}
          \drawblackedge{20}{1}
          \drawblackedge{20}{2}
          \drawmetalvertconnection{20}{2}{4}
          \drawviabetween{20}{1}
          \drawviabetween{20}{5}
          \drawmetalhorconnection{20}{21}{1}
          \drawmetalhorconnection{20}{21}{2}

          \drawgreyvertexstack{24}{5}
          \drawblackedge{24}{2}
          \drawmetalhorconnection{24}{25}{2}

          \draw [<-, red] (\Cmetalvertpitch*10,8) -- (\Cmetalvertpitch*11,2.5);
          \draw [<-, red] (\Cmetalvertpitch*14,2.5) -- (\Cmetalvertpitch*15,2.5);
          \draw [<-, red] (\Cmetalvertpitch*18,2.5) -- (\Cmetalvertpitch*19,2.5);
          \draw [<-, red] (\Cmetalvertpitch*22,2.5) -- (\Cmetalvertpitch*23,2.5);
        \end{scope}
        }

      \end{tikzpicture}
    }
  \end{figure}
\end{frame}

\begin{frame}[t]{Routing by dynamic program}
  Propagate tree at each VTrack, and prune. Place greedily.
  \begin{figure}[ht]
    \only<2>{\includegraphics[width=\textwidth]{images/tree-010}}
    \only<3>{\includegraphics[width=\textwidth]{images/tree-020}}
    \only<4>{\includegraphics[width=\textwidth]{images/tree-030}}
    \only<5>{\includegraphics[width=\textwidth]{images/tree-040}}
    \only<6>{\includegraphics[width=\textwidth]{images/tree-050}}
    \only<7>{\includegraphics[width=\textwidth]{images/tree-060}}
    \only<8>{\includegraphics[width=\textwidth]{images/tree-070}}
    \only<9>{\includegraphics[width=\textwidth]{images/tree-080}}
    \only<10>{\includegraphics[width=\textwidth]{images/tree-090}}
    \only<11>{\includegraphics[width=\textwidth]{images/tree-100}}
    \only<12>{\includegraphics[width=\textwidth]{images/tree-110}}
    \only<13>{\includegraphics[width=\textwidth]{images/tree-120}}
    \only<14>{\includegraphics[width=\textwidth]{images/tree-130}}
    \only<15>{\includegraphics[width=\textwidth]{images/tree-140}}
    \only<16>{\includegraphics[width=\textwidth]{images/tree-150}}
    \only<17>{\includegraphics[width=\textwidth]{images/tree-160}}
  \end{figure}
\end{frame}

\SetKwFunction{FNextPos}{NextCellPosInGrid}
\SetKwFunction{FRouteTo}{RouteToTrack}
\SetKwFunction{FTryAllExtensions}{TryAllExtensions}
\SetKwFunction{FTryAllMetalVertAndViaCombinations}{TryAllMetalVertAndViaCombinations}
\SetKwFunction{FTryAllViaBelows}{TryAllViaBelows}
\SetKwFunction{FTryToAddSlice}{TryToAddSlice}
%\SetKwFunction{FCanPlaceMetalVertNet}{CanPlaceMetalVertNet}
\SetKw{KwGoTo}{goto}
\SetKwProg{Fn}{function}{:}{end}
\SetKwInOut{Input}{Input}
\SetKwInOut{Output}{Output}

\section{The Algorithm}
\begin{frame}{Overview of the dynamic program}
  \begin{itemize}
    \item Before starting:
    \begin{itemize}
      \item Model global right border pins as dummy leaf cell
      \item Set global left border pins in routing tree root
    \end{itemize}
    \item Outside loop (\textbf{Algorithm \autoref{prog:single_row_toplevel}}): For each cell:
      \begin{itemize}
        \item Place it at the leftmost plausible position
        \item Try to propagate tree to end of cell (\textbf{Algorithm \autoref{prog:route_to}})
        %\item Propagate tree to just before proposed cell position (\textbf{Algorithm \autoref{prog:route_to}})
        %\item Try to propagate tree to end of cell (also \textbf{Algorithm \autoref{prog:route_to}})
        \item Go back and try different position if that fails
      \end{itemize}
  \end{itemize}
  \vfill
  \begin{itemize}
    \item Each time the tree is propagated by one track:
      \begin{itemize}
        \item For each leaf of the tree:
          \begin{itemize}
            \item Try out all extensions, checking for legality and domination
          \end{itemize}
      \end{itemize}
  \end{itemize}
  \vfill
  \textbf{Note:} all variables assumed to be global for ease of notation
\end{frame}

\begin{frame}[plain]%{Pseudocode (I): Top-level algorithm}
  \small
  \begin{algorithm}[H]
  \caption{Single-row top level algorithm}\label{prog:single_row_toplevel}
  \Input{An instance of the \textsc{Track Optimisation Problem}}
  \Output{placement $p$, routing tree $\mathcal{B}_t$ containing an optimal routing}
  $\mathcal{B}_{-1} := \{R_{\textup{init}}\}$\tcp*{start with single slice at VTrack -1}
  $t := -1$\tcp*{initialise current track}
  $p : \emptyset \to \mathbb{N}_0$\tcp*{start with empty partial placement}
  \For{leaf cell index $i=1,\ldots,n$}{
    $P_i :=$ first track in the placement grid that is not too close to the previous cell/left border\;
    \FRouteTo{$P_i - 1$}\;\label{prog:nextpos}
    Set $p(i) := P_i$\;
    \FRouteTo{$P_i + W_i - 1$}\;
    \If{$\mathcal{B}_t$ is empty}{
      $t := P_i - 1$\tcp*{move back to VTrack before cell}
      Unset $p(i)$\;
      $P_i :=$ first track right of $P_i$ lying in cell $i$'s placement grid\;
      \KwGoTo\ref{prog:nextpos}\;
    }
  }
  \KwRet $(p,\ \mathcal{B}_t)$\;
  \end{algorithm}
\end{frame}

\begin{frame}[plain]
\small
\begin{algorithm}[H]
  \caption{Advance routing tree from VTrack $t$ to $t_{\textup{goal}}$}\label{prog:route_to}
  \Fn{\FRouteTo{$t_{\textup{goal}}$}}{
    \uIf{$t_{\textup{goal}} \ge P_i$ and cell placement illegal due to a blocked track or short with existing wiring}{
      $t := t_{\textup{goal}}$, $\mathcal{B}_t := \emptyset$\tcp*{do not even try to route}
    }
    \Else{
      \While{$t < t_{\textup{goal}}$}{
        $t := t + 1$\;
        $\mathcal{B}_t := \emptyset$\;
        %\tcp{pre-compute various data used later} % (all but 2 omitted since they only give minor speed-ups):}
        \ForEach{$R_{\prev}\in\mathcal{B}_{t-1}$}{
          \ForEach{legal extension $R$ of $R_{\prev}$}{
            $\mathcal{B}_t := \mathcal{B}_t \cup \{R\}$\;
            \ForEach{$R'\in\mathcal{B}_t$}{
              \uIf{$R'$ dominates $R$}{
                $\mathcal{B}_t := \mathcal{B}_t \setminus \{R\}$\;
              }
              \ElseIf{$R$ dominates $R'$}{
                $\mathcal{B}_t := \mathcal{B}_t \setminus \{R'\}$\;
              }
            }
          }
        }
      }
    }
  }
\end{algorithm}
\end{frame}

\begin{frame}{Enumerating legal extensions of $R$}\label{frame:enumerating_extensions}
  First, just continue outgoing nets of $R$. Then enumerate possible out-net changes and \viabelow{}, \metalvert{} and
  \viabetween{} shapes:
  \vfill
  \begin{itemize}
    \item Try up to one \metalhor{} track change {\color{cyan}($\le {k\choose 2}$)}
    \item Try stopping and starting nets on HTracks {\color{cyan}($\le \mathcal{O}(1)^k$)}
    \item Try placing \metalvert{} of relevant nets, with \viabetween{} connections (unless we already have a track
      change) {\color{cyan}($\le \mathcal{O}(1)$)}
    \item Try different combinations of \viabelow{} shapes {\color{cyan}($\le 2^{\mathcal{O}(1)}$)}
  \end{itemize}
  {
    \color{cyan} $\leadsto \mathcal{O}({k\choose 2}\cdot\mathcal{O}(1)^k\cdot\mathcal{O}(1)\cdot 2^{\mathcal{O}(1)})
    = \mathcal{O}(1)$ extensions enumerated
  }
  \vfill
  Finally, check legality within constant-width window.
\end{frame}

\begin{frame}{Domination checks {\transparent{0.5}(slightly simplified)}}
\begin{itemize}
  \item Uses \emph{domination information}, which contains
    \begin{itemize}
      \item Conflicting future via shapes \uncover<2->{{\color{ForestGreen} ``$\subseteq$''}}
      \item \metalhor{} right line-ends and nets at $\nu_t$ \uncover<2->{{\color{ForestGreen} ``$=$''}}
      \item Lower bounds on next \metalhor{} line-ends after $\nu_t$ \uncover<2->{{\color{ForestGreen} ``$\le$''}}
      \item Pins that have been connected to \uncover<2->{{\color{ForestGreen} ``$\supseteq$''}}
      \item Netlength \uncover<2->{{\color{ForestGreen} ``$\le$''}}
    \end{itemize}
  \end{itemize}
  \begin{figure}[h]
    \centering
    \resizebox{0.8\textwidth}{!}{%
      \begin{tikzpicture}
        \drawmetalhortrackpattern{3}{3}
        \drawmetalverttrackpattern{3}{3}
        \labelmetalverttrack{1}{$t$}
        \drawnutline{1}{3}
        %\labelmetalhortrack{1}{HTrack $s$}
        \drawviabetween{2}{1}
        \drawmetalhorconnection{2}{3}{1}
        \drawblackpath{2}{3}{1}

        \drawmetalhorconnection{1}{2}{2}
        %\drawmetalhorconnectionlabelsbelow{2}{5}{1}{$\lambda$}{}
        \draw [red, <->] (\Cmetalvertpitch*2-\Cmetalhoroverhangpastvtrack,\Cmetalhorpitch*0.5) -- node[below]{\hphantom{MMMMMMMMMMMMM}\scriptsize extension of \metalhor{} shape past edge} (\Cmetalvertpitch*2,\Cmetalhorpitch*0.5);%
      \end{tikzpicture}
    }
  \end{figure}
  \uncover<2->{{\color{ForestGreen} In green: necessary relations for LHS to dominate RHS}}
\end{frame}

\section{Running Time}
\begin{frame}{Running time}
Divide running time into 3 factors:
\vfill
\begin{itemize}
  \item \# iterations: {\color{cyan} $\mathcal{O}((\sum_{i=1}^n W_i) + B\max_{i=1}^n W_i^2)$}
  \pause
  \item \# routing tree nodes per VTrack: {\color{cyan} $\mathcal{O}(1)$}
    \uncover<4->{
    \begin{itemize}
      \item $\mathcal{O}(1)$ different slices possible at any single VTrack
      \item $\mathcal{O}(1)$ relevant pins at any VTrack

        $\Rightarrow$ $\mathcal{O}(1)$ different sets of connected pins
      \item Only rightmost $\mathcal{O}(1)$ slices of a routing relevant for its domination information, apart from cost
      \item[$\Rightarrow$] After domination checks, only $\mathcal{O}(1)$ partial routings survive
    \end{itemize}
    }
  \pause
  \item Time needed to iterate over extensions: {\color{cyan} $\mathcal{O}(1)$}

  (as already seen in slide \autoref{frame:enumerating_extensions})
    %\uncover<5->{
    %\begin{itemize}
    %  \item $k = \mathcal{O}(1)$ HTracks
    %  \item $\mathcal{O}(1)$ relevant nets
    %  \item $\mathcal{O}(1)$ pinshapes to choose from
    %\end{itemize}
    %}
\end{itemize}
\end{frame}

\begin{frame}{Bounding the number of iterations}
Without backtracking, this would just be the total solution width.
\vfill
\pause
\begin{itemize}
  \item Split iterations into \textcolor{ForestGreen}{contributing} and \textcolor{red}{wasted} iterations
  \item Recall that $W_i$ is the width of cell $i$
  \item Let $w_i$ be the size of the gap preceding cell $i$
\end{itemize}

$\Rightarrow$ Total number of iterations:
$\mathcal{O}({\color{ForestGreen} (\sum_{i=1}^n w_i + W_i)} + {\color{red} (\sum_{i=1}^n w_i W_i)}) =
\mathcal{O}(\sum_{i=1}^n(w_i + 1)(W_i + 1))$
\vfill
\pause
Bounds on the $w_i$:
\begin{itemize}
  \item Without any VTrack blockages: $w_i = \mathcal{O}(1)$
  \item Each VTrack blockage increases some $w_i$ by up to $W_i$
\end{itemize}
\vfill
$\Rightarrow$ given $B$ blockages, the number of iterations is bounded by
\(\mathcal{O}\left((\sum_{i=1}^n W_i) + B\max_{i=1}^n W_i^2\right)\)
\end{frame}


\section{Multibit}
\begin{frame}{Multibit}
The algorithm can be modified to handle multi-bit cells:
\begin{itemize}
  \item Maintain partial routing trees for each bit
  \item As before, iterate over all the cells in correct order
  \item Only move on to cell $i+1$ when all bits of cell $i$ have been routed to the end
\end{itemize}
\vfill
\pause
\textbf{Running time:}
Letting $H_i$ be the number of bits of cell $i$ and $H$ the global number of bits, this gives a running time of
$\mathcal{O}\left({\color{ForestGreen}((\sum_{i=1}^n W_i) + B\max_{i=1}^n W_i) H}
+ {\color{red}B \max_{i=1}^n W_i^2 H_i}\right)$, which
is bounded by \[\mathcal{O}\left(\left[\left(\sum_{i=1}^n W_i\right) + B \max_{i=1}^n W_i^2\right]\cdot H\right).\]
\end{frame}

\section{Extra features}
\begin{frame}{Optimality vs. running time trade-off}
\begin{itemize}
  \item Allow $R_1$ to dominate $R_2$ if $\textup{cost}(R_1) \le \textup{cost}(R_2) {\color{red}+ \Delta_c}$
  \item Theoretical worst netlength increase: $\Delta_c \cdot (\textup{solution width})$
\end{itemize}
\begin{table}[ht]
\resizebox{0.8\textwidth}{!}{
\begin{tabular}{c|l||lllll}
& \color{red}$\Delta_c$ & \color{red}0 & \color{red}60 & \color{red}120 & \color{red}600 & \color{red}$\infty$ \\
\hline
\hline
\multirow{4}{*}{\makecell{F\\ (26)%TESTCASE\_LOGIC\_SIMPLE\_MC\_40
}} & time {[}s{]} & 17.1 & 9.3 & 6.6 & 4.7 & 4.7 \\
 & cost {[}nm{]} & 4532.0 & 4643.0 & 4753.0 & 5165.0 & 5165.0 \\
 & increase (abs.) & 0.0 & 111.0 & 221.0 & 633.0 & 633.0 \\
 & increase (rel.) & 0.0\% & 2.4\% & 4.9\% & 14.0\% & 14.0\% \\
\hline
\multirow{4}{*}{\makecell{E\\ (26)%TESTCASE\_LOGIC\_SIMPLE\_MC\_40\_Red
}} & time {[}s{]} & 23.0 & 12.4 & 8.3 & 5.7 & 5.7 \\
 & cost {[}nm{]} & 4532.0 & 4643.0 & 4691.0 & 5206.5 & 5206.5 \\
 & increase (abs.) & 0.0 & 111.0 & 159.0 & 674.5 & 674.5 \\
 & increase (rel.) & 0.0\% & 2.4\% & 3.5\% & 14.9\% & 14.9\% \\
\hline
\multirow{4}{*}{\makecell{A\\ (27)%TALK
}} & time {[}s{]} & 7.0 & 4.8 & 3.8 & 2.8 & 2.8 \\
 & cost {[}nm{]} & 4394.5 & 4496.5 & 4589.5 & 5267.0 & 5267.0 \\
 & increase (abs.) & 0.0 & 102.0 & 195.0 & 872.5 & 872.5 \\
 & increase (rel.) & 0.0\% & 2.3\% & 4.4\% & 19.9\% & 19.9\% \\
\hline
\multirow{4}{*}{\makecell{D\\ (32)%TESTCASE\_LOGIC\_SIMPLE\_40
}} & time {[}s{]} & 69.3 & 39.3 & 30.9 & 18.6 & 18.5 \\
 & cost {[}nm{]} & 5351.5 & 5415.5 & 5597.5 & 5863.5 & 5863.5 \\
 & increase (abs.) & 0.0 & 64.0 & 246.0 & 512.0 & 512.0 \\
 & increase (rel.) & 0.0\% & 1.2\% & 4.6\% & 9.6\% & 9.6\% \\
\hline
\multirow{4}{*}{\makecell{B\\ (212)%TESTCASE\_LATCH\_40
}} & time {[}s{]} & 12.7 & 10.9 & 10.4 & 8.6 & 8.6 \\
 & cost {[}nm{]} & 24321.0 & 24548.5 & 24678.0 & 24926.5 & 24926.5 \\
 & increase (abs.) & 0.0 & 227.5 & 357.0 & 605.5 & 605.5 \\
 & increase (rel.) & 0.0\% & 0.9\% & 1.5\% & 2.5\% & 2.5\% \\
\hline
\end{tabular}
}
\end{table}
\end{frame}

\begin{frame}{Potential future work}
\vfill
Of practical interest:
\begin{itemize}
  \item Route on \metalbelow{}
  \item Primitive routing between bits
\end{itemize}
\vfill
Less likely to be implemented:
\begin{itemize}
  \item Loosen routing restrictions (e.g. starting nets arbitrarily early, drop/loosen topology restriction)
  \item Future costs
  \item Placement decentralisation, Dijkstra-esque tree traversal
\end{itemize}
\vfill
\end{frame}

\begin{frame}[plain]
  \large
  Thank you!
  \begin{figure}
    \centering
    \includegraphics[width=0.9\textwidth]{images/TresHombres.jpg}
  \end{figure}
\end{frame}

\end{document}
