thesis.pdf: thesis.tex tikz_commands.tex sources.bib #images/*.pdf
	pdflatex thesis.tex
	bibtex thesis
	pdflatex thesis.tex
	pdflatex thesis.tex

.PHONY: tidy clean_all
tidy:
	rm -fv *.aux *.log *.toc *.out *.bbl *.blg
clean_all: tidy
	rm -fv *.dvi *.pdf *.man
